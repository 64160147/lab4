import java.util.Scanner;

public class Array8 {
    public static void main(String[] args) {
        int arr[] = new int [3];
        Scanner sc = new Scanner(System.in);

        for (int i=0;i<arr.length;i++){
            System.out.print("Please input arr[" + i + "] = ");
            arr[i] = sc.nextInt();
        }
        System.out.print("arr = ");
        for (int i=0;i<arr.length;i++){
            System.out.print(arr[i] + " ");
        }
        System.out.println();
        int sum =0;
        for (int i=0;i<arr.length;i++){
            sum = sum + arr[i];
        }
        System.out.println("sum = "+sum);

        double avg = ((double)sum)/arr.length;
        System.out.println("avg = "+avg);  
        
        int min = 0;
        for (int i=1;i<arr.length;i++){
            if(arr[min]>arr[i]){
                min = i;
            }
        }
        System.out.println("min = "+arr[min]);

        int max = 0;
        for (int i=1;i<arr.length;i++){
            if(arr[max]<arr[i]){
                max = i;
            }
        }
        System.out.println("max = "+arr[max]);
        
    }
}
