import java.util.Scanner;

public class MyApp {
    public static void main(String[] args) {
        while (true) {
            printWelcome();
            printMenu();
            int choice = inputChoice();
            switch (choice){
                case 1:
                    printHelloWorldNTime();
                    break;
                case 2:
                    addTwoNumber();
                    break;
                case 3:
                    exitProgram();
            }
        }
    }

    static void exitProgram() {
        System.out.println("Bye!!");
        System.exit(0);
    }

    static void addTwoNumber() {
        int first = inputFirst();
        int second = inputSecond();
        int result = add(first,second);
        System.out.println("Result = " +  result);
    
    }

    static int add(int first, int second) {
        return first + second;
    }

    static int inputSecond() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input first number: ");
        int first = sc.nextInt();
        return first;
    }

    static int inputFirst() {
        Scanner sc = new Scanner(System.in);
        System.out.print("Please input second number: ");
        int second = sc.nextInt();
        return second;
    }

    static void printHelloWorldNTime() {
        int time = InputPrintTime(); 
        for (int i = 0 ; i<time ; i++ ) {
            System.out.println("Hello World!!!");
        }
    }

    static int InputPrintTime() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input time: ");
            int time = sc.nextInt();
            if(time >= 1){
                return time;
            } else {
                System.out.println("Error!!! : Please input time in Positive number!!!");
            }
        }
        
    }

    static int inputChoice() {
        Scanner sc = new Scanner(System.in);
        while(true){
            System.out.print("Please input your choice(1-3): ");
            int choice = sc.nextInt();
            if (choice >=1 && choice <=3){
              return choice;  
            } else {
                System.out.println("Error!!! : Please input choice between 1-3!!!");
            }
        }
    }

    static void printWelcome() {
        System.out.println("Welcome to my app!!!");
    }

    static void printMenu() {
        System.out.println("--Menu--");
        System.out.println("1. Print Hello World N times");
        System.out.println("2. Add 2 number");
        System.out.println("3. Exit");
    }
    
}
